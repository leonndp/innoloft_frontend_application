import React from 'react'

import classes from './HeaderMenuItem.module.css'

const HeaderMenuItem = ({ children, href, alert }) => {

    return (
        <a href={href} className={classes.headerMenuItem}>
            {children}
            {alert && <div className={classes.alert}></div>}
        </a>
    )
}

export default HeaderMenuItem