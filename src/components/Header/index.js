import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGlobeAmericas, faEnvelope, faBell } from '@fortawesome/free-solid-svg-icons'

import Logo from './../../assets/images/energieloft_logo.png'
import HeaderMenuItem from './HeaderMenuItem'

import classes from './Header.module.css'

const Header = () => {

    return (
        <div className={classes.header}>
            <a href="#"><img className={classes.logo} src={Logo} /></a>
            {/* <FontAwesomeIcon icon={faGlobeAmericas} color="whitesmoke" /> */}
            <ul className={classes.menuContent}>
                <li><HeaderMenuItem href="#"><FontAwesomeIcon icon={faGlobeAmericas} color="white" /> EN</HeaderMenuItem></li>
                <li><HeaderMenuItem href="#" alert={true}><FontAwesomeIcon icon={faEnvelope} color="white" /></HeaderMenuItem></li>
                <li><HeaderMenuItem href="#" alert={true}><FontAwesomeIcon icon={faBell} color="white" /></HeaderMenuItem></li>
            </ul>
        </div>
    )
}

export default Header